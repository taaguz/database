show tables;

DROP DATABASE investmentServicesGroup12;
CREATE DATABASE investmentServicesGroup12;

USE investmentServicesGroup12;
CREATE  TABLE IF NOT EXISTS roles (
  `role_id` int NOT NULL auto_increment primary KEY ,
  `role_title` VARCHAR(150) NOT NULL );

  USE investmentServicesGroup12;
 CREATE  TABLE IF NOT EXISTS credentials (
  `user_name` VARCHAR(150) NOT NULL,
  `password` VARCHAR(150) NOT NULL,
  `role_num` int,
  PRIMARY KEY(user_name),
  FOREIGN KEY(role_num) REFERENCES roles(role_id)); 
  

select * from deals;
  
  
  USE investmentServicesGroup12;
  CREATE  TABLE IF NOT EXISTS instruments (
  `instrument_id` int NOT NULL auto_increment primary KEY ,
  `instrument_name` VARCHAR(150) NOT NULL );

USE investmentServicesGroup12;
   CREATE  TABLE IF NOT EXISTS counterparties (
  `cpty_id` int NOT NULL auto_increment primary KEY ,
  `cpty_name` VARCHAR(150) NOT NULL );
  

USE investmentServicesGroup12;
     CREATE  TABLE IF NOT EXISTS deal_types (
  `type_id` int NOT NULL auto_increment primary KEY ,
  `type_name` VARCHAR(1) NOT NULL );
  

  USE investmentServicesGroup12;
   CREATE TABLE IF NOT EXISTS deals (
  `deal_id` int NOT NULL auto_increment  primary KEY,
  `instrument` int NOT NULL,
  `cpty` int NOT NULL,
  `deal_type`varchar(1) NOT NULL,
  `price` FLOAT NOT NULL,
  `quantity` int,
  `deal_time` varchar(150),
  FOREIGN KEY(instrument) REFERENCES instruments(instrument_id),
  FOREIGN KEY(cpty) REFERENCES 	counterparties(cpty_id));
  
INSERT INTO roles (role_title) VALUES ('Security Officer');
INSERT INTO roles (role_title) VALUES ('Trader');
INSERT INTO roles (role_title) VALUES ('System Architect');
INSERT INTO roles (role_title) VALUES ('Senior Trader');

INSERT INTO counterparties (cpty_name) VALUES ('Lewis');
INSERT INTO counterparties (cpty_name) VALUES ('Selvyn');
INSERT INTO counterparties (cpty_name) VALUES ('Richard');
INSERT INTO counterparties (cpty_name) VALUES ('Lina');
INSERT INTO counterparties (cpty_name) VALUES ('John');

INSERT INTO counterparties (cpty_name) VALUES ('Nidia');
